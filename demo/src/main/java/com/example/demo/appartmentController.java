package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController

public class appartmentController {

    @Autowired
    RestRepository restRepository;

    @GetMapping("/appartment")
    public List<appartment> getAllAppartments()
    {
        return (List<appartment>) restRepository.findAll();
    }

    @PostMapping("/appartment")
    public appartment createAppartment( @RequestBody appartment appartment) {
        return restRepository.save(appartment);
    }

    @DeleteMapping("/appartment/{id}")
    public ResponseEntity<?> deleteappartment(@PathVariable(value = "id") Long id) {
        appartment appartment = restRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(""));

        restRepository.delete(appartment);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/appartment/{id}")
    appartment updateapp(@RequestBody appartment newapp, @PathVariable Long id) {

        return restRepository.findById(id).map(appartment -> {
            appartment.setGroup_type(newapp.getGroup_type());
            appartment.setClim(newapp.getClim());
            appartment.setBaby(newapp.getBaby());
            appartment.setNbr_lit(newapp.getNbr_lit());
            appartment.setSurface(newapp.getSurface());
            appartment.setPrix(newapp.getPrix());
            return restRepository.save(appartment);
        }).orElseGet(() -> {
            newapp.setId(id);
            return restRepository.save(newapp);
        });
    }



}
