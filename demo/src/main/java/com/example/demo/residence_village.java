package com.example.demo;

import javax.persistence.*;
import java.util.Set;

@Entity
public class residence_village {


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getEspagne_France() {
        return Espagne_France;
    }

    public void setEspagne_France(Boolean espagne_France) {
        Espagne_France = espagne_France;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getLieu() {
        return Lieu;
    }

    public void setLieu(String lieu) {
        Lieu = lieu;
    }

    public Boolean getWifi() {
        return wifi;
    }

    public void setWifi(Boolean wifi) {
        this.wifi = wifi;
    }

    public Boolean getPiscine() {
        return piscine;
    }

    public void setPiscine(Boolean piscine) {
        this.piscine = piscine;
    }

    public Boolean getSpa() {
        return spa;
    }

    public void setSpa(Boolean spa) {
        this.spa = spa;
    }

    public Boolean getGarderie() {
        return garderie;
    }

    public void setGarderie(Boolean garderie) {
        this.garderie = garderie;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<appartment> appartments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean Espagne_France ;

    private String Region ;

    private String Lieu ;

    private Boolean wifi;

    private Boolean piscine ;

    private Boolean spa ;

    private Boolean garderie ;
}
