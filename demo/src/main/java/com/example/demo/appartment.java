package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class appartment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getGroup_type() {
        return group_type;
    }

    public void setGroup_type(Boolean group_type) {
        this.group_type = group_type;
    }

    public Boolean getClim() {
        return clim;
    }

    public void setClim(Boolean clim) {
        this.clim = clim;
    }

    public Boolean getBaby() {
        return baby;
    }

    public void setBaby(Boolean baby) {
        this.baby = baby;
    }

    public Long getNbr_lit() {
        return nbr_lit;
    }

    public void setNbr_lit(Long nbr_lit) {
        this.nbr_lit = nbr_lit;
    }

    public Long getSurface() {
        return surface;
    }

    public void setSurface(Long surface) {
        this.surface = surface;
    }

    public Long getPrix() {
        return prix;
    }

    public void setPrix(Long prix) {
        this.prix = prix;
    }

    private Boolean group_type ;

    private Boolean clim ;

    private Boolean baby ;

    private Long nbr_lit;

    private Long surface;

    private Long prix;

}
