package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class residencevillageController {

    @Autowired
    RestVillageRepo restVillageRepo;

    @GetMapping("/revi")
    public List<residence_village> getAllReVi()
    {
        return (List<residence_village>) restVillageRepo.findAll();
    }

    @PostMapping("/revi")
    public residence_village createReVi( @RequestBody residence_village residence_village) {
        return restVillageRepo.save(residence_village);
    }

    @DeleteMapping("/revi/{id}")
    public ResponseEntity<?> deleterevi(@PathVariable(value = "id") Long id) {
        residence_village residence_village = restVillageRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(""));

        restVillageRepo.delete(residence_village);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/revi/{id}")
    residence_village updaterevi(@RequestBody residence_village newrevi, @PathVariable Long id) {

        return restVillageRepo.findById(id).map(residence_village -> {
            residence_village.setEspagne_France(newrevi.getEspagne_France());
            residence_village.setRegion(newrevi.getRegion());
            residence_village.setLieu(newrevi.getLieu());
            residence_village.setWifi(newrevi.getWifi());
            residence_village.setPiscine(newrevi.getPiscine());
            residence_village.setSpa(newrevi.getSpa());
            residence_village.setGarderie(newrevi.getGarderie());
            return restVillageRepo.save(residence_village);
        }).orElseGet(() -> {
            newrevi.setId(id);
            return restVillageRepo.save(newrevi);
        });
    }



}
